/**
 * @file main.c
 * @author tzq
 * @brief
 * @version 0.1
 * @date 2022-06-07
 *
 * @copyright Copyright (c) 2022
 *
 * 队列的的基本操作!
 * 通常，称进数据的一端为 "队尾"，出数据的一端为 "队头"，数据元素进队列的过程称为 "入队"，出队列的过程称为 "出队"。
 * 不仅如此，队列中数据的进出要遵循 "先进先出" 的原则，即最先进队列的数据元素，同样要最先出队列。
 */
#include <stdio.h>

int enQueue(int *a, int rear, int data)
{
    printf("入队: %d rear: %d\n", data, rear);
    a[++rear] = data;
    return rear;
}

void deQueue(int *a, int top, int rear)
{
   
   while (top != rear)
   {
       printf("出队:%d top:%d \n", a[++top], top);
   }
  
}

int main()
{
    printf("%d \n",2%3);
    int a[100];
    int top, rear;
    top = rear = 0;
    rear = enQueue(a, rear, 1);
    rear = enQueue(a, rear, 2);
    rear = enQueue(a, rear, 3);
    rear = enQueue(a, rear, 4);

    deQueue(a,top,rear);
    return 0;
}
/**
 * @file main.c
 * @author tzq
 * @brief
 * @version 0.1
 * @date 2022-06-07
 *
 * @copyright Copyright (c) 2022
 *
 * 队列的的基本操作!
 * 通常，称进数据的一端为 "队尾"，出数据的一端为 "队头"，数据元素进队列的过程称为 "入队"，出队列的过程称为 "出队"。
 * 不仅如此，队列中数据的进出要遵循 "先进先出" 的原则，即最先进队列的数据元素，同样要最先出队列。
 */
#include <stdio.h>

#define SIZE 5

int enQueue(int *a, int top, int rear, int data) {
    //判断队列是否已经满了，如果数组满了将其从 a[0] 开始存储。
    if (((rear + 1) % SIZE) == top) {
        printf("队列已经满了\n");
        return rear;
    }

    printf("入队: %d\n", data);
    a[rear % SIZE] = data;
    rear++;
    return rear;
}

int deQueue(int *a, int top, int rear) {
    if (top == rear % SIZE) {
        printf("队列为空!\n");
        return top;
    }
    printf("出队: %d \n", a[top]);
    top = (top + 1) % SIZE;
    return top;
}

int main() {
    int a[SIZE];
    int top, rear;
    top = rear = 0;
    //入队
    rear = enQueue(a, top, rear, 1);
    rear = enQueue(a, top, rear, 2);
    rear = enQueue(a, top, rear, 3);
    rear = enQueue(a, top, rear, 4);

    //出队
    top = deQueue(a, top, rear);
    top = deQueue(a, top, rear);
    top = deQueue(a, top, rear);
    top = deQueue(a, top, rear);
    top = deQueue(a, top, rear);
    top = deQueue(a, top, rear);

    rear = enQueue(a, top, rear, 5);
    rear = enQueue(a, top, rear, 5);
    rear = enQueue(a, top, rear, 1000);
    top = deQueue(a, top, rear);
    top = deQueue(a, top, rear);
    top = deQueue(a, top, rear);

    printf("\ntop: %d \n", top);
    return 0;
}
#include <stdio.h>
#include "tree.h"

//队头和队尾
int front = 0, rear = 0;

//入队
void en_queue(TreeNode **list, TreeNode *node) {
    list[++rear] = node;
}

//出队
TreeNode *de_queue(TreeNode **list) {
    if (rear > front) {
        return list[++front];
    }
}

int main() {
    //创建一棵树
    Tree tree;
    create_tree(&tree);

    //声明一个数组作为队列
    TreeNode *list[20];

    //把首节点入队
    en_queue(list, tree);
    //设置一个临时指针
    TreeNode *p;

    //检查队尾的指针大于队头指针则继续遍历
    while (rear > front) {
        //出栈
        p = de_queue(list);
        display_tree_node(p);

        if (p->left_child != NULL) {
            en_queue(list, p->left_child);
        }

        if (p->right_child != NULL) {
            en_queue(list, p->right_child);
        }

    }
    printf("\n");
    printf("Hello, World!\n");
    return 0;
}

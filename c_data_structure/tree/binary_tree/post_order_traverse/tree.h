//
// Created by tzq on 2022/7/1.
//

#ifndef POST_ORDER_TRAVERSE_TREE_H
#define POST_ORDER_TRAVERSE_TREE_H

// 定义一个树的数据类型
#define DataElem int

// 定义一个树节点类型
typedef struct TreeNode {
        //数据
        DataElem data;
        //左右节点
        struct TreeNode *left_child,*right_child;
} TreeNode,*Tree;


void create_tree(Tree *tree);

void display_tree_node(TreeNode *t);

#endif //POST_ORDER_TRAVERSE_TREE_H

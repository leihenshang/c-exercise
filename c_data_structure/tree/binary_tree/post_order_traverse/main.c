#include <stdio.h>
#include "tree.h"

//后序遍历
void post_order_traverse(Tree t) {
    if (t) {
        post_order_traverse(t->left_child);
        post_order_traverse(t->right_child);
        display_tree_node(t);
    }
}

int main() {
    Tree tree;
    create_tree(&tree);
    post_order_traverse(tree);
    return 0;
}

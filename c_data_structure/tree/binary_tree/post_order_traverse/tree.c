//
// Created by tzq on 2022/7/1.
//
#include "tree.h"
#include <stdlib.h>
#include <stdio.h>

//创建一棵树
/*
      1
    /   \
   2     3
 /  \   / \
4    5 6   7
 */
void create_tree(Tree *t) {

    //        1
    //      /   \
    //     2     3
    //   /  \   / \
    //  4    5 6   7

    //先为初始的树分配内存
    (*t) = (TreeNode *) malloc(sizeof(TreeNode));
    (*t)->data = 1;

    //左边的子树
    (*t)->left_child = (TreeNode *) malloc(sizeof(TreeNode));
    (*t)->left_child->data = 2;
    (*t)->left_child->left_child = (TreeNode *) malloc(sizeof(TreeNode));
    (*t)->left_child->left_child->data = 4;
    (*t)->left_child->left_child->left_child = NULL;
    (*t)->left_child->left_child->right_child = NULL;
    (*t)->left_child->right_child = (TreeNode *) malloc(sizeof(TreeNode));
    (*t)->left_child->right_child->data = 5;
    (*t)->left_child->right_child->left_child = NULL;
    (*t)->left_child->right_child->right_child = NULL;

    //右边的子树
    (*t)->right_child = (TreeNode *) malloc(sizeof(TreeNode));
    (*t)->right_child->data = 3;
    (*t)->right_child->left_child = (TreeNode *) malloc(sizeof(TreeNode));
    (*t)->right_child->left_child->data = 6;
    (*t)->right_child->left_child->left_child = NULL;
    (*t)->right_child->left_child->right_child = NULL;
    (*t)->right_child->right_child = (TreeNode *) malloc(sizeof(TreeNode));
    (*t)->right_child->right_child->data = 7;
    (*t)->right_child->right_child->left_child = NULL;
    (*t)->right_child->right_child->right_child = NULL;
}

//打印一个节点数据
void display_tree_node(TreeNode *t) {
    printf(" %d ", t->data);
}
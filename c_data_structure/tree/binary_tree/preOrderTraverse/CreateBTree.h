//
// Created by tzq on 2022/5/5.
//

#ifndef PREORDERTRAVERSE_CREATEBTREE_H
#define PREORDERTRAVERSE_CREATEBTREE_H
#define TElemType int

typedef struct BNode {
    TElemType data;
    struct BNode *lchild, *rchild; //定义左节点和右节点
} BNode, *BTree;


void CreateBTree(BTree *T);

#endif //PREORDERTRAVERSE_CREATEBTREE_H

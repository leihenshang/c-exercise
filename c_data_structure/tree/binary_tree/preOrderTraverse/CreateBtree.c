// Created by tzq on 2022/5/5.
//
#include "CreateBTree.h"
#include <stdlib.h>

void CreateBTree(BTree *T) {
    *T = (BNode *) malloc(sizeof(BNode));
    (*T)->data = 1;
    (*T)->lchild = (BNode *) malloc(sizeof(BNode));
    (*T)->rchild = (BNode *) malloc(sizeof(BNode));

    (*T)->lchild->data = 2;
    (*T)->lchild->lchild = (BNode *) malloc(sizeof(BNode));
    (*T)->lchild->rchild = (BNode *) malloc(sizeof(BNode));
    (*T)->lchild->lchild->data = 4;
    (*T)->lchild->lchild->lchild = NULL;
    (*T)->lchild->lchild->rchild = NULL;
    (*T)->lchild->rchild->data = 5;
    (*T)->lchild->rchild->lchild = NULL;
    (*T)->lchild->rchild->rchild = NULL;

    (*T)->rchild->data = 3;
    (*T)->rchild->lchild = (BNode *) malloc(sizeof(BNode));
    (*T)->rchild->rchild = (BNode *) malloc(sizeof(BNode));
    (*T)->rchild->lchild->data = 6;
    (*T)->rchild->lchild->lchild = NULL;
    (*T)->rchild->lchild->rchild = NULL;
    (*T)->rchild->rchild->data = 7;
    (*T)->rchild->rchild->lchild = NULL;
    (*T)->rchild->rchild->rchild = NULL;
}
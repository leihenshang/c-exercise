//
// Created by tzq on 2022/7/1.
//

#ifndef PRE_ORDER_TRAVERSE_TWO_TREE_H
#define PRE_ORDER_TRAVERSE_TWO_TREE_H

// 定义一个树的数据类型
#define DataElem int

// 定义一个树节点类型
typedef struct TreeNode {
        //数据
        DataElem data;
        //左右节点
        struct TreeNode *left_child,*right_child;
} TreeNode,*Tree;


void create_tree(Tree *tree);

#endif //PRE_ORDER_TRAVERSE_TWO_TREE_H

//
// Created by tzq on 2022/6/1.
//

#ifndef LINKED_TABLE_REVERSE_H
#define LINKED_TABLE_REVERSE_H

#endif //LINKED_TABLE_REVERSE_H

//定义一个单链表结构
typedef struct Link {
    //数据域
    int data;
    //指针域，指向下一个节点
    struct Link *next;
    //link作为节点名，一个节点都是一个link结构体
} link;

link *iteration_reverse(link *head);
link *recursive_reverse(link *head);
link *head_reverse(link *head);
link *local_reverse(link *head);

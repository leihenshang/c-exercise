#include <stdio.h>
#include <string.h>

void Next(char *T, int *next) {
    int i = 1;
    next[1] = 0;
    int j = 0;
    while (i < strlen(T)) {
        if (j == 0 || T[i - 1] == T[j - 1]) {
            i++;
            j++;
            next[i] = j;
        } else { j = next[j]; }
    }
}

int KMP(char *S, char *T) {
    int next[10];
    Next(T, next);
    //根据模式串 T,初始化 next 数组
    int i = 1;
    int j = 1;
    while (i <= strlen(S) &&
           j <= strlen(T)) { //j==0:代表模式串的第一个字符就和当前测试的字符不相等；S[i-1]==T[j-1],如果对应位置字符相等，两种情况下，指向当前测试的两个指针 下标 i 和 j 都向后移
        if (j == 0 || S[i - 1] == T[j - 1]) {
            i++;
            j++;
        } else {
            j = next[j];//如果测试的两个字符不相等，i 不动，j 变为当前测试字符串的 next 值
        }
    }
    if (j > strlen(T)) {//如果条件为真，说明匹配成功
        return i - (int) strlen(T);
    }
    return -1;
}

int main() {
//    int j = 0;
//    int arr[2] = {1,2};
//    printf("arr:%d j-1= %d \n",arr[(j-1)],j-1);
//    return 0;

    int i = KMP("ababcabcacbab", "abcac");
    printf("%d", i);
    return 0;
}
#include <stdio.h>
#include <string.h>

static int global_i = 0;

//匹配子串，返回子串出现在主串中的位置，如果返回-1，则没有匹配到。
int match(char *main_str, char *sub_str) {
    int i = 0, j = 0;
    while (i < strlen(main_str) && j < strlen(sub_str)) {
        global_i++;
        if (main_str[i] == sub_str[j]) {
            i++;
            j++;
        } else {
            //notice: 为什么不是 i + j + 1 ?
            //如果是上面这个的话，比如 主串：abcdecdefde，子串:cdef,从0开始，匹配到下标4时,子串也匹配到2了。如果不减去j,那么主串就是从5开始匹配了。而不是从c开始的3开始。相当于跳过主串的前面字符串
            i = i - j + 1;
            j = 0;
        }
    }

    if (j >= strlen(sub_str)) {
        return i - strlen(sub_str) + 1;
    }


    return -1;
}

int main() {
    char main_str[40] = "tzq hhaha1h,my name is ae!";
    char sub_str[40] = "ha1";
    int location = match(main_str, sub_str);
    printf("global_i : %d \n", global_i);
    printf("main: %s\n sub: %s \n", main_str, sub_str);
    if (location > 0) {
        printf("子串在主串中开始的位置为: %d \n", location);
        return 0;
    }

    printf("没有匹配到数据！\n");
    return -1;
}

#include <stdio.h>

//入栈,a 是栈存储，top 是栈顶指针，elem 是入栈元素
int push(int *a, int top, int elem) {
    a[++top] = elem;
    printf("入栈:%d\n", elem);
    return top;
}

//出栈，a 是栈存储，top是栈顶指针
int pop(int *a, int top) {
    if (top == -1) {
        printf("空栈");
        return -1;
    }

    printf("出栈：%d \n", a[top]);
    top--;
    return top;
}

int main() {
    //声明一个栈顶指针
    int top = -1;
    //用数组设置一个栈存储
    int stack_data[5];
    top = push(stack_data, top, 1);
    top = push(stack_data, top, 2);
    top = push(stack_data, top, 3);
    top = push(stack_data, top, 4);

    top = pop(stack_data, top);
    top = pop(stack_data, top);
    top = pop(stack_data, top);
    top = pop(stack_data, top);
    top = pop(stack_data, top);
    return 0;
}
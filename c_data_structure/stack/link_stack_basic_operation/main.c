/**
 * 用链表实现栈
 */
#include <stdio.h>
#include <stdlib.h>

//定义一个链表结构体
typedef struct Link {
    //数据
    int data;
    //指针
    struct Link *next;
} link;

link *push(link *head, int data) {
    link *temp = head;
    link *new = (link *) malloc(sizeof(link));
    new->data = data;
    new->next = NULL;
    if (head == NULL) {
        temp = new;
    } else {
        new->next = temp;
        temp = new;
    }
    printf("入栈: %d\n", data);

    return temp;
}

link *pop(link *head) {
    if (head == NULL) {
        printf("空栈\n");
        return head;
    }
    link *temp = head;
    head = head->next;
    printf("出栈:%d\n", temp->data);
    free(temp);
    temp = NULL;
    return head;
}

int main() {
    link *head = NULL;
    head = push(head, 1);
    head = push(head, 2);
    head = push(head, 3);
    head = push(head, 4);
    head = push(head, 4);

    head = pop(head);
    head = pop(head);
    head = pop(head);
    head = pop(head);
    head = pop(head);
    head = pop(head);
    return 0;
}
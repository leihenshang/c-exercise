#include <stdlib.h>
#include <stdio.h>

//打印数组
void print_arr(int a[], int len)
{
    for (int i = 0; i <= len; i++)
    {
        printf(" %d ", a[i]);
    }
    printf("\n");
}

int main()
{
    //初始化一个乱序数组
    int a[] = {10, 9, 1, 2, 22, 55, 3, 6, 8, 88};
    //先打印一下数组的数据
    print_arr(a, 9);

    //初始化变量，l 设为0, r 设为数组的长度, temp 用于保存临时下标
    int l = 0, r = 9, temp = 0;
    int count = 0;
    //使用for循环，从左到右遍历数组的元素
    for (int i = l; i <= r; i++)
    {
        count++;
        //标记是否还需要继续遍历，每次遍历都把临时下标置为0，避免出现错误
        temp = 0;

        // 为什么j+1 ? 每次冒泡的时候都是从第一个数开始的，所以需要跳过第一个，第一个就是自己。
        // 为什么要 "数组长度 - i" ? 冒泡排序，逐渐将大的数移动到最右侧。随着遍历的次数增加，右边都是大的数，不需要再进行比较。
        for (int j = l; (j + 1) <= (r - i); j++)
        {
            //设置一个变量保存临时的数值,用于变量值交换
            // a = 1,b = 2 交换两者的值
            // temp = 0
            // temp = a; a = b; b = temp;
            int temp_val = 0;
            if (a[j] > a[j + 1])
            {
                count++;
                temp_val = a[j + 1];
                a[j + 1] = a[j];
                a[j] = temp_val;

                // 如果置1说明有需要排序的
                temp = 1;
            }
        }
        if (temp == 0)
        {
            break;
        }
    }

    //再次打印数组的数据，进行肉眼的对比 orz!!!
    print_arr(a, 9);
    printf("执行了 %d 次\n", count);
    return 0;
}
 
cmake_minimum_required(VERSION 3.0.0)
project(qsort C)

set(CMAKE_C_STANDARD 99)

add_executable(qsort main.c)